package imp.logback

import imp.util.impClean
import imp.util.logger
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.nio.file.Paths
import kotlin.io.path.deleteIfExists
import kotlin.io.path.isRegularFile
import kotlin.io.path.readText

class LogbackTest {
	@Test fun test() {
		val testDir = Paths.get("test-data")
		testDir.impClean()

		val cfgFile = testDir.resolve("test-log-cfg.xml")
		val logFile = testDir.resolve("test.log")
		cfgFile.deleteIfExists()
		logFile.deleteIfExists()

		initLogbackCreateConfigIfMissing(cfgFile, "/test-log-cfg.xml", LogbackTest::class)
		assertTrue("config file should be created: $cfgFile", cfgFile.isRegularFile())

		val log = LogbackTest::class.logger
		log.trace("This line will be skipped in the file.")
		log.info("This line will be logged in the file.")
		Thread.sleep(2000)
		shutdownLogback()

		assertTrue("log file should be created: $logFile", logFile.isRegularFile())
		val allLogs = logFile.readText()
		assertTrue("Info level log wasn't found.", allLogs.contains("logged"))
		assertFalse("Trace level log was found.", allLogs.contains("skipped"))
	}
}