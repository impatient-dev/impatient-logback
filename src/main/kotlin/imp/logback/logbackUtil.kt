package imp.logback

import ch.qos.logback.classic.BasicConfigurator
import ch.qos.logback.classic.Level
import ch.qos.logback.classic.LoggerContext
import ch.qos.logback.classic.joran.JoranConfigurator
import ch.qos.logback.classic.layout.TTLLLayout
import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.encoder.LayoutWrappingEncoder
import ch.qos.logback.core.util.StatusPrinter
import imp.util.logger
import org.apache.commons.io.FileUtils
import org.apache.commons.io.IOUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.nio.file.Files
import java.nio.file.Path
import kotlin.reflect.KClass
import kotlin.system.exitProcess

private class LogbackUtil

private fun logbackContext(): LoggerContext = LoggerFactory.getILoggerFactory() as LoggerContext

/**
 * Initializes Logback. This should work even if you've already created some loggers,
 * but ideally this function is called at the beginning of the main method..
 * @param cfgFile the location of the (user-editable) logging config file, such as logback.xml
 * @param srcIfMissing the path to the classpath resource representing the default config, such as "/logback-default.xml".
 * This must represent a file in src/main/resources. If the config file does not exist, this function will create it from this source.
 * @param someAppClass some class in your application; we just use this to call cls.getResourceAsStream()
 */
fun <C: Any> initLogbackCreateConfigIfMissing(cfgFile: Path, srcIfMissing: String, someAppClass: KClass<C>) {
	if (Files.exists(cfgFile)) {
		println("Log config file: $cfgFile")
	} else {
		println("Creating default log config file at $cfgFile from $srcIfMissing")
		someAppClass.java.getResourceAsStream(srcIfMissing).use { ins ->
			if (ins == null)
				throw RuntimeException("Resource not found: $srcIfMissing")
			val lines = IOUtils.readLines(ins, Charsets.UTF_8)
			FileUtils.writeLines(cfgFile.toFile(), lines)
		}
	}
	initLogbackContext { context ->
		val joran = JoranConfigurator()
		joran.context = context
		joran.doConfigure(cfgFile.toFile())
	}
	LogbackUtil::class.logger.debug("Logback initialized from {}.", cfgFile)
}

/**Initializes Logback to print all output to the console, instead of using a config file to configure Logback. Intended for use in tests.*/
fun initLogbackBasic() {
	initLogbackContext { context -> BasicConfigurator().configure(context) }
	LogbackUtil::class.logger.debug("Logback initialized (basic).")
}

/**Initializes Logback to print all output to the console, including TRACE-level logging.*/
fun initLogbackTrace() {
	initLogbackContext { context ->
		val ca = ConsoleAppender<ILoggingEvent>().also { it.context = context; it.name = "console" }
		val encoder = LayoutWrappingEncoder<ILoggingEvent>().also { it.context = context }
		val layout = TTLLLayout().also { it.context = context; it.start() }
		encoder.layout = layout
		ca.encoder = encoder
		ca.start()
		val root = context.getLogger(Logger.ROOT_LOGGER_NAME)
		root.level = Level.TRACE
		root.addAppender(ca)
	}
	LogbackUtil::class.logger.debug("Logback initialized (trace).")
}

private fun initLogbackContext(cfg: (LoggerContext) -> Unit) {
	val context = logbackContext()
	context.reset()
	cfg(context)
	StatusPrinter.printInCaseOfErrorsOrWarnings(context)
}


/**Shuts down Logback logging.*/
fun shutdownLogback() {
	LogbackUtil::class.logger.info("Shutting down Logback.")
	logbackContext().stop()
}
/**Shuts down logging and terminates the process. Call this when your program wants to terminate, but is not crashing due to an error.*/
fun shutdownLogbackAndExitNormally() {
	LogbackUtil::class.logger.info("Shutting down logging and terminating process normally.")
	logbackContext().stop()
	exitProcess(0)
}
/**Shuts down logging and terminates the process. Call this when your program needs to crash due to an error.*/
fun shutdownLogbackAndExitError(exitCode: Int = 1) {
	LogbackUtil::class.logger.info("Shutting down logging and terminating process with exit code {}.", exitCode)
	logbackContext().stop()
	exitProcess(exitCode)
}