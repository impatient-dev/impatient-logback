plugins {
	id("org.jetbrains.kotlin.jvm")
}

group = "imp-dev"
version = "0.0.0"

repositories {
	mavenCentral()
}

dependencies {
	implementation(project(":impatient-utils"))

	api("ch.qos.logback:logback-classic:1.2.8")

	testImplementation(project(":impatient-junit4"))
}