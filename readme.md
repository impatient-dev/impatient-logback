# impatient-logback

A very simple project that

* Has logback-classic as a dependency, with "api" so your project can also use it directly if you want to.

* Has methods to start up and shut down Logback.

That's it. Functions that only need SLF4J are in impatient-utils, not this project.